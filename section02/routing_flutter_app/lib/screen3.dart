import 'package:flutter/material.dart';

class Screen3 extends StatefulWidget{
  @override
  _Screen3State createState() => _Screen3State();
}

class _Screen3State extends State<Screen3>{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Screen3"),
          actions: <Widget>[
            new IconButton(
                icon: new Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.pop(context);
                },
                ),
          ],
        ),
      )
    );
  }
}