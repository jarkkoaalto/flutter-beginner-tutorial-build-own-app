import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:ui' as ui;

void main() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]
  ).then((_) => runApp(InputPage()));
}

class InputPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputPageState();
  }
}

class InputPageState extends State<InputPage> {

  @override
  Widget build(BuildContext context){
    return MaterialApp(
      home: Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(top: 40.0, left: 8.0),
        child: Column(
          children: <Widget>[
            Text("Column.1", textDirection: TextDirection.ltr),
            Text("Column 2", textDirection: TextDirection.ltr),
            Text("Column 3", textDirection: TextDirection.ltr),
            Row(
              textDirection: TextDirection.ltr,
              children: <Widget>[
                Text("Row 1", textDirection: TextDirection.ltr),
                Text("Row 2", textDirection: TextDirection.ltr),
                Text("Row 3", textDirection: TextDirection.ltr),
              ],
            )
          ],
        ),
      )
      ),
    );

  }
}