import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then(
    (_) => runApp(
          MaterialApp(
            debugShowCheckedModeBanner: false,
            home: InputPage(),
            theme: ThemeData(
              primaryColor: Colors.red,
              accentColor: Colors.lime,
              brightness: Brightness.dark,
            ),
          ),
        ),
  );
}

class InputPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputPageState();
  }
}

class InputPageState extends State<InputPage> {
  bool checkInput = false;

  int gender = 1;

  printData(String input) {
    print(input);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Flutter Beginner",
            textDirection: TextDirection.ltr,
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.beenhere),
              onPressed: () {
                print("icon");
              },
            ),
            IconButton(
              icon: Icon(Icons.map),
              onPressed: () {
                print("icon");
              },
            )
          ],
        ),
        drawer: Drawer(
            child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Image(image: AssetImage("images/rain-455124_640.jpg")),
                Padding(
                  padding: EdgeInsets.only(top: 30.0, left: 16.0),
                  child: CircleAvatar(
                    radius: 40.0,
                    backgroundImage: AssetImage('images/drawer_image.jpeg'),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 120.0, left: 16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("etunimi sukunimi",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                              color: Colors.white)),
                      Text("flutter-tutorials-example@gmail.com",
                          style: TextStyle(color: Colors.white)),
                    ],
                  ),
                )
              ],
            ),
            ListView(
              shrinkWrap: true,
              children: <Widget>[
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Hello There'),
                    onTap: () {}),
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Hello There'),
                    onTap: () {}),
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Hello There'),
                    onTap: () {}),
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Hello There'),
                    onTap: () {}),
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Hello There'),
                    onTap: () {}),
                ListTile(
                    leading: Icon(Icons.add),
                    title: Text('Hello There'),
                    onTap: () {}),
              ],
            )
          ],
        )),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            print("fab");
          },
        ),
        body: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: TextField(
                decoration: InputDecoration(
                    icon: Icon(Icons.person), hintText: "Username"),
                onChanged: (input) {
                  print(input);
                },
              ),
            ),
            Checkbox(
              value: checkInput,
              onChanged: (bool value) {
                setState(() {
                  checkInput = value;
                  print(value);
                });
              },
            ),
            ButtonBar(
              children: <Widget>[
                Radio(
                  value: 1,
                  groupValue: gender,
                  onChanged: (int value){
                    setState(() {
                      gender = value;
                      print(gender);
                    });
                  },
                ),
                Radio(
                  value: 2,
                  groupValue: gender,
                  onChanged: (int value){
                    setState(() {
                      gender = value;
                      print(gender);
                    });
                  },
                ),
              ],
            )
          ],
        ));
  }
}
