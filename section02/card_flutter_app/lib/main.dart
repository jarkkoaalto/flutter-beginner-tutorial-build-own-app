import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


void main() {
  SystemChrome.setPreferredOrientations(
          [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown])
      .then((_) => runApp(
       InputPage()
  ),
  );
}

class InputPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return InputPageState();
  }
}

class InputPageState extends State<InputPage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Padding(
          padding: const EdgeInsets.only(top: 40.0, left: 8.0, right: 8.0),
          child: Column(
            children: <Widget>[
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Row(
                    textDirection: TextDirection.ltr,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.person,
                            textDirection: TextDirection.ltr),
                        onPressed: () {
                          print("person");
                        },
                      ),
                      Expanded(
                        child: Text("Child 2", textDirection: TextDirection.ltr),
                      ),
                      IconButton(
                        icon: Icon(Icons.add, textDirection: TextDirection.ltr),
                        onPressed: () {
                          print("add");
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Row(
                    textDirection: TextDirection.ltr,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(Icons.person,
                            textDirection: TextDirection.ltr),
                        onPressed: () {
                          print("person");
                        },
                      ),
                      Expanded(
                        child: Text("Child 2", textDirection: TextDirection.ltr),
                      ),

                      IconButton(
                        icon: Icon(Icons.add, textDirection: TextDirection.ltr),
                        onPressed: () {
                          print("add");
                        },
                      ),
                    ],
                  ),
                ),
              ),
              Image(
            image: AssetImage("images/images.jpeg"),
    )
            ],
          ),
        ),
      ),
    );
  }
}
