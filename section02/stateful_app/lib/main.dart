import 'package:flutter/material.dart';

void main() => runApp(MyStatefulWidget());

class MyStatefulWidget extends StatefulWidget {

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();

}

class _MyStatefulWidgetState extends State<MyStatefulWidget>{

  var myColor = Colors.red;

  changeColor(){
    setState((){

    });
    print (myColor);
    myColor = Colors.blue;
    print(myColor);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            body: Container(
              color: Colors.red,
              child: Center(
                child: RaisedButton(
                  child: Text("Click", textDirection: TextDirection.ltr,),
                  onPressed: () {
                    print("Hi from myStatefulWidget");
                  },
                ),
              ),
            ),

        ),
    );
  }
}