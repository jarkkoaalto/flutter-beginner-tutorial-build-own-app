import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]).then(
    (_) => runApp(
          MaterialApp(
            debugShowCheckedModeBanner: false,
            home: MyApp(),
            theme: ThemeData(
              primaryColor: Colors.red,
              // accentColor: Colors.lime,
              brightness: Brightness.dark,
            ),
          ),
        ),
  );
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  // This widget is the root of your application.
  List list = List();

  String input = "";

  @override
  void initState() {
    super.initState();
    list.add("nr.1");
    list.add("nr.2");
    list.add("nr.3");
    list.add("nr.4");
    list.add("nr.5");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Todos",
          textDirection: TextDirection.ltr,
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("Add ToDo"),
                  content: TextField(
                    onChanged: (String value) {
                      input = value;
                    },
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("add"),
                      onPressed: () {
                        setState(() {
                          list.add(input);
                        });
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
              },
            );
          }),
      body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
                key: Key(list[index]),
                child: ListTile(
                    title: Text(list[index])
                ),
              onDismissed: (direction){
                  setState((){
                    list.removeAt(index);
                  });
              } ,
            );
          }),
    );
  }
}
