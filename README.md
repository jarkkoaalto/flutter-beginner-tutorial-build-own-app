# Flutter Beginner Tutorial  Build own App #

Learn the dart and flutter basics

## Section 01 Introducrtion ##
This section inculdes Dart programming basics

## Section 02 Flutter ##
This section includes Flutter programming basics

### First app ###
This is basics centered textview and change packground color red

![Screenshot](01_basics.png)

### Second app ###
This app demo centered button

![Screenshot](02_button.png)

### Third app ###
Demonstrated how to add Card - element and build row element with icons and text. And how to add image

![Screenshot](04_Card_pic.png)

### Fourth app ###
This app demonstrated how to create Drawer

![Screenshot](05_body.png) ![screenshot](05_Drawer.png)

### Fifth app ###
This app demonstrated how to build route between three pages and cread appbar navigation arrows

![Screenshot](06_1routing.png)
![Screenshot](06_2routing.png)
![Screenshot](06_3routing.png)

### Seventh app ###
Demonstrated how to creat listview and how to handle add / remove functionality

![Screenshot](07_listview.png)
![Screenshot](07_2listview.png)


